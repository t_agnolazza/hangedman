/**
 * @file hanged.c
 * @brief It contains the two function of the routine of the game
 *
 * @author T.Agnolazza
 * @version 1.0
**/

/**
 * @brief Choosing the word
 * @detail It asks the user to insert the word to guess. 
 *		   It sends the word to the other player.
 *		   It receives and prints every choice of the other player until he guess or loose all his lives.
 *
 * @param socket an open socket
 *
 * @return 'w' if the other did not guess, 'n' otherwise; 'e' if there were error
**/
char chooseWord(int socket);

/**
 * @brief Guessing the word
 * @detail It receives the word form the other player.
 *		   It enters a loop, which ends if the player guesses the word or he lost all of his lives.
 *		   The player can either chooses to guess the word or to guess a letter.
 *		   Every miss is a life lost.
 *		   Every choices is registered into a string buffer and sent to the other player.
 *
 * @param socket an open socket
 *
 * @return 'w' if you guess, 'n' otherwise; 'e' if there were error
**/
char guess(int socket);
