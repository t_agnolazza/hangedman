//Uncomment to define: enter to a special mode
//#define DEBUG

//Connection parameters
#define PORT 7980

//Avarege paramenters
#define DIM_BUFFER 51
#define STR_DIM_BUFFER "51"

#define ADD_LENGTH 16 //xxx.xxx.xxx.xxx [16 char]

#define DIM_NAME 11
#define STR_DIM_NAME "11"

#define TRUE 1
#define FALSE 0

/** 
 * @file core.c
 * @brief It implements a graphic version of the game Hangedman.
 *
 * @detail It is the main part of the program. 
 *		   It recalls an external *.txt to show the rules of the game.
 *		   It is divided into two parts: the first contains the instructions for the connection as a host,
 *		   the latter contains the instructions for connection as a client (either needed on different pc).
 *		   They both calls the same routine of the game, which can be ended at every cycle by user input (host or client)).  
 * 
 * @author T.Agnolazza
 * @version 1.1
 * @since 1.1
 *
**/

//Include basic libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Include network libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>

//Include error library
#include <errno.h>

//Include game library
#include "hanged.h"
//Include personalized library for sending/receiving strings through socket
#include "net.h"

//Game functions (part of the routine)
char chooseWord(int socket);
char guess(int socket);

//Prototypes of network functions (in "net.h")
int sendTo(const int socket, char *buf, int length);
int recFrom(const int socket, char *buf, int length);
int sendCtrl(const int socket, const char code);
char recCtrl(const int socket);
int changingPort(struct sockaddr_in *addr);
int getIpList(void);

//Error definitions
void errNet(int errorCode);

//Only print functions
void mainTitle();
void showScore();

//Struct of a player
struct player{
	char name[DIM_NAME];
	int score;
	int guesser;
} p1, p2;


/**
 * @brief It sets the connection if the player chooses to be the host or the client.
 *		  It can resolve port problems, quite automatically.
 *		  It starts the routine of the game (divaded into two phases) untill user choice to end.
 *
 * @return 0 if there were no problems, -1 if something went wrong
 *
 * @version 1.1
 * @since 1.0
**/
int main(void) {

	//Intro screen
	mainTitle();
	
#ifdef DEBUG
	printf("---DEBUG MODE---\n\n");
#endif
	
	//Services variables
	char *buf = (char *) malloc(DIM_BUFFER*sizeof(char));
	memset(buf, '\0', DIM_BUFFER*sizeof(char));
	int end = FALSE;	
	char ans = '\0';
	char temp;

	//Filling player.name with EQUAL blank number
	for(int i = 0; i < DIM_NAME*sizeof(char); i++){
		p1.name[i] = ' ';
		p2.name[i] = ' ';
	}
	
	//Variables for conntection
	int sockHost, sockClient;
	struct sockaddr_in addrHost, addrClient;
	
	//Giving the rules to unexpert player
	printf("Do you know the rules of this game (y/n)? ");
	scanf("%1s", &ans);
	
	//Ignoring useless character
	while((temp = getchar()) != '\n' && (temp != '\r')) {}
	
	if(ans == 'n'){
		FILE *fp;
		char flow;
		
		//Opening "rules.txt" in reading mode
		if((fp = fopen("../rules.txt","r")) != NULL){
			//Printing the content of the file
			while((flow = getc(fp)) != EOF) {
				printf("%c",flow);
			}
			fclose(fp);
		}
		else{
			printf("I cannot find the file with the rules.\n\n\n");
		}
		memset(buf, '\0', DIM_BUFFER*sizeof(char));
	}
	
	else if(ans != 'y'){
		printf("I can not understand you.\n\n");
	}
	
	//Choosing who is the host
	printf("Are you the host or the client (h/c)? ");
	scanf("%1s", &ans);
	while((temp = getchar()) != '\n' && (temp != '\r')) {}
	
	//Choosing host
	if((ans == 'h') || (ans == 'H')) {
	
	#ifdef DEBUG	
		printf("\n*******************\n");
		printf("**** CONNECTED ****\n");
		printf("********HOST*******\n");
		printf("version 1.0\n\n");
	#endif
		
		//Initializing a socket for the host
		if ((sockHost = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
			errNet(errno);
			printf("Error creating a socket.\n\n");
			free(buf);
			return -1;
		}
		
		//Setting host parametres
		addrHost.sin_family = AF_INET;
		addrHost.sin_port = htons(PORT);
		addrHost.sin_addr.s_addr = htonl(INADDR_ANY); //it accepts any incoming address
	
	#ifdef DEBUG
		printf("Debug info on server: \n");
		printf("---Socket:%d\n", sockHost);
		printf("---Port:%d\n", ntohs(addrHost.sin_port));
		printf("---Address:%s\n\n", inet_ntoa(addrHost.sin_addr));
	#endif
		
		//Printing host ipv4 address to give to the client
		printf("\n");
		if (getIpList() < 0) {
			printf("Error retrieving ipv4 address from your pc.\n\n");
		}
		
		//It binds the socket to the address
		if (bind(sockHost, (struct sockaddr*) &(addrHost), sizeof(struct sockaddr_in)) < 0) {
			
			//Changing port in case the default port is unavaiables
			while(TRUE){
				errNet(errno);
				printf("Error binding.\n\n");
				printf("Maybe actual port is not avaiable, would you like to try with another one (y/n)? ");
				scanf("%1s", &ans);
 				while((temp = getchar()) != '\n' && (temp != '\r')) {}		
 				
 				if(ans == 'y'){
 					if (changingPort(&addrHost) < 0){
 						continue;
 					}
 					if (bind(sockHost, (struct sockaddr*) &(addrHost), sizeof(struct sockaddr)) >= 0){
 						break;
 					}
 				}
 				
 				else {	
					close(sockHost);
					free(buf);
					return -1;
				}
			}
		}
				
		//Listening to an incoming connection
		if (listen(sockHost, 1) < 0) {
			errNet(errno);
			printf("Error listening.\n\n");
			close(sockHost);
			free(buf);
			return -1;
		}
	
		//Accepting the incoming connection
		int sockLen = sizeof(struct sockaddr_in);	
		if ((sockClient = accept(sockHost, (struct sockaddr *) &addrClient, &sockLen)) < 0) {
			errNet(errno);
			printf("Error accepting connection.\n\n");
			close(sockHost);
			free(buf);
			return -1;
		} 
		
	#ifdef DEBUG
		struct sockaddr_in addrProv;
		if(getpeername(sockClient, (struct addr *) &addrProv, &sockLen) < 0){
			close(sockHost);
			close(sockClient);
			free(buf);
			return -1;
		}
		
		printf("---Address connected: %s\n\n", inet_ntoa(addrProv.sin_addr));
	
		if (recFrom(sockClient, buf, DIM_BUFFER) < 0){
			close(sockHost);
			close(sockClient);
			free(buf);
			return -1;
		}
	
		printf("%s\n", buf);
	
		memset(buf, '\0', DIM_BUFFER*sizeof(char));
		strcpy(buf, "--- Host: connection checked!!");	
		
		printf("%s\n\n", buf);	
		if (sendTo(sockClient, buf, strlen(buf)) < 0) {
			close(sockHost);
			close(sockClient);
			free(buf);
			return -1;
		}
	#endif
	
	
		//Starting the game 
		
		//Setting intial host palyer data
		printf("\nTell me your name: ");
		scanf("%"STR_DIM_NAME"[A-z]*s", p1.name);
		while((temp = getchar()) != '\n' && (temp != '\r')) {}
		
		p1.score = 0;
		p1.guesser = FALSE;
		
		//Sending the name to the other player
		if(send(sockClient, p1.name, strlen(p1.name)*sizeof(char), 0) < (strlen(p1.name)*sizeof(char))) {
			errNet(errno);
			close(sockClient);
			close(sockHost);
			free(buf);
			return -1;
		}
		
		//Receiving the name from the other player
		if(recFrom(sockClient, buf, DIM_BUFFER) < 0) {
			close(sockClient);
			close(sockHost);
			free(buf);
			return -1;
		}
		
		//Setting initial client player data
		strncpy(p2.name, buf, strlen(p2.name)*sizeof(char));
		p2.score = 0;
		p2.guesser = TRUE;
		
		
		printf("Your enemy's name is %s.\n\n", p2.name);
		showScore();
		
		printf("We can start! \n");
		
		
		//Starting the game loop between the two phases of the game
		//p1 is the host
		//p2 is the client
		
		//While the game does not end...
		while(!end) {
		
			//p1 has to choose a word
			if(!p1.guesser){
				
				ans = chooseWord(sockClient);
			}
			
			//p1 has to guesss the word
			else{
			
				ans = guess(sockClient);
			}

			if(ans=='l'){
				printf("Nope! \n\n");
				p2.score++;
			}
			else if (ans=='w') {
				printf("You get a point!\n\n");
				p1.score++;
			}
			else if(ans=='e'){
				printf("Something went wrong.\n\n");
				close(sockClient);
				return -1;
			}	

			
			//Setting new turn
			p1.guesser = !p1.guesser;			
			
			//Show the score
			showScore();
			
			
			//End the routine
			printf("Would you like to end (y/n)? ");
			scanf("%1s", buf);

			while((temp = getchar()) != '\n' && (temp != '\r')) {}
			
			//Sending the answer to the client			
			if(sendCtrl(sockClient, buf[0]) < 0){
				close(sockClient);
				close(sockHost);
				free(buf);
				return -1;
			}
			
			if((ans = recCtrl(sockClient)) < 0){
				close(sockClient);
				close(sockHost);
				free(buf);
				return -1;
			}
			
		#ifdef DEBUG
			printf("--- ans = %c\n", ans);
			printf("--- buffer[0] = %c\n\n", buf[0]);
		#endif
		
			//Comparing both answers
			if((ans=='y')||(ans=='Y')||(buf[0]=='y')||(buf[0]=='Y')){
				if(p1.score > p2.score){
					printf("\n\n\tYOU WIN!\n\n");
				}
				else if (p1.score < p2.score){
					printf("\n\n\tYOU, LOOOOSER!\n\n");
				}
				else {
					printf("\n\n\tDICE!\n\n");
				}
				end = TRUE;
				
				if (close(sockHost) < 0) {
					errNet(errno);
					printf("Error closing the host.\n\n");
					free(buf);
					return -1;
				}
			}
			memset(buf, '\0', DIM_BUFFER*sizeof(char));	
		}
	}	
		
		
	//Choosing client
	else if ((ans == 'c') || (ans == 'C')) {

		//Addictional variables for the client;
		int sockLen = sizeof(struct sockaddr_in);	
		char ipHost[ADD_LENGTH] = "";
			
	#ifdef DEBUG
		printf("\n*******************\n");
		printf("**** CONNECTED ****\n");
		printf("******CLIENT*******\n");
		printf("version 1.0\n\n");
	#endif
	
		//Initializing a socket for the client
		if ((sockClient = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
			errNet(errno);
			printf("Error creating the socket.\n\n");
			free(buf);
			return -1;
		}
		
		//Setting default protocol and port
		addrHost.sin_family = AF_INET;
		addrHost.sin_port = htons(PORT);
		
		//Insert host address
		printf("Please enter host IPaddress (xxx.xxx.xxx.xxx): ");
		scanf("%15[0-9.]*s", ipHost);
		
		while((temp = getchar()) != '\n' && (temp != '\r')) {}
			
		
		addrHost.sin_addr.s_addr = inet_addr(ipHost); 
		
	#ifdef DEBUG
		printf("\nDebug info on client: \n");
		printf("---Socket:%d\n",sockClient);
		printf("---Port:%d\n",ntohs(addrHost.sin_port));
		printf("---Address:%s\n\n",inet_ntoa(addrHost.sin_addr));
	#endif
	
		//Establishing the connection to the host
		if (connect(sockClient, (struct sockaddr *) &addrHost, sizeof(struct sockaddr)) < 0) {
			errNet(errno);
			printf("Error connecting.\n\n");
			int connected = FALSE;
			
			//Trying other ports
			printf("Would you like me to try other possible ports (y/n)? ");
			scanf("%1s", &ans);
			while((temp = getchar()) != '\n' && (temp != '\r')) {}					
			
			if(ans == 'y'){
				short port = IPPORT_RESERVED;
				while(port < IPPORT_USERRESERVED){
					addrHost.sin_port = htons(port++);
					if (connect(sockClient, (struct sockaddr *) &addrHost, sizeof(struct sockaddr)) >= 0) {
						connected = TRUE;
						break;
					}						
				}
			}
			if(connected == FALSE){
				close(sockClient);
				free(buf);
				return -1;
			}	
		}
	
	#ifdef DEBUG
		strcpy(buf, "--- Client: connection checking...");
		printf ("%s\n", buf);
		
		if (sendTo(sockClient, buf, strlen(buf)) < 0) {
			close(sockClient);
			free(buf);
			return -1;
		}
		
		if (recFrom(sockClient, buf, DIM_BUFFER) < 0){
			close(sockClient);
			free(buf);
			return -1;
		}

		printf("%s\n\n", buf);		
	#endif
	
	
		//Starting the game 
		
		//Setting intial client data
		printf("\nTell me your name: ");
		scanf("%"STR_DIM_NAME"[A-z]*s",p2.name);
		while((temp = getchar()) != '\n' && (temp != '\r')) {}
		
		p2.score = 0;
		p2.guesser = TRUE;
		
		//Receiving name from the host
		if(recFrom(sockClient, buf, DIM_BUFFER) < 0) {
			close(sockClient);
			free(buf);
			return -1;
		}
		
		//Sending name to the host
		if(send(sockClient, p2.name, strlen(p2.name)*sizeof(char), 0) < (strlen(p2.name)*sizeof(char))) {
			close(sockClient);
			errNet(errno);
			free(buf);
			return -1;
		}
		
		//Setting initial host data
		strncpy(p1.name, buf, strlen(p1.name)*sizeof(char));
		p1.score = 0;
		p1.guesser = FALSE;
		
		printf("Your enemy's name is %s.\n\n", p1.name);
		showScore();
			
		printf("We can start! \n");
		
		
		//Starting the game loop between the two phases of the game
		// p1 is the host
		// p2 is the client
		
		//While the game does not end...
		while(!end) {
		
			//p2 has to guess
			if(!p2.guesser){
				
				ans = chooseWord(sockClient);				
			}
			
			else{
			
				ans = guess(sockClient);
			}				
				
			if(ans=='l'){
				printf("Nope! \n\n");
				p1.score++;
			}
			else if (ans=='w') {
				printf("You get a point!\n\n");
				p2.score++;
			}
			else if(ans=='e'){
				printf("Something went wrong.\n\n");
				close(sockClient);
				free(buf);
				return -1;
			}
							
			//Setting new turn
			p2.guesser = !p2.guesser;
			
			//Show the score
			showScore();
			
			//End the routine
			printf("Would you like to end (y/n)? ");
			scanf("%1[y+n]s", buf);
			
			while((temp = getchar()) != '\n' && (temp != '\r')) {}
			
			//Retrieving the answer from the host
			if((ans = recCtrl(sockClient)) < 0){
				close(sockClient);
				free(buf);
				return -1;
			}			
			
			if(sendCtrl(sockClient, buf[0]) < 0){
				close(sockClient);
				free(buf);
				return -1;
			}
			
		#ifdef DEBUG
			printf("--- ans = %c\n", ans);
			printf("--- buffer[0] = %c\n\n", buf[0]);
		#endif
			
			//compare both answer
			if((ans=='y')||(ans=='Y')||(buf[0]=='y')||(buf[0]=='Y')){
				if(p2.score > p1.score){
					printf("\n\n\tYOU WIN!\n\n");
				}
				else if (p2.score < p1.score){
					printf("\n\n\tYOU, LOOOOSER!\n\n");
				}
				else {
					printf("\n\n\tDICE!\n\n");
				}
				end = TRUE;
			}
			
			memset(buf, '\0', DIM_BUFFER*sizeof(char));
		}
	}
	
	//Do not choose
	else {
		printf("I do not understand you.\n\n");
		free(buf);
		return 0;
	}	
		
	if (close(sockClient) < 0) {
		errNet(errno);
		printf("Error closing the socket.\n\n");
		free(buf);
		return -1;
	}
	
	free(buf);
	return 0;
}	


/**
 * @brief Intro screen for the game
 *
 * @return void
**/

void mainTitle() {
	printf("**************************************************************\n");
	printf("** TTTTT  H   H  EEEEE                                      **\n");
	printf("**   T    H   H  E                                          **\n");
	printf("**   T    HHHHH  EEE                                        **\n");	
	printf("**   T    H   H  E                                          **\n");	
	printf("**   T    H   H  EEEEE                                      **\n");	
	printf("**************************************************************\n");
	printf("**************************************************************\n");
	printf("** H  H    A    N  N   GGG   EEEE  DD    M   M    A    N  N **\n");
	printf("** H  H   A A   NN N  G      E     D D   MM MM   A A   NN N **\n");
	printf("** HHHH  AAAAA  N NN  G  GG  EEE   D  D  M M M  AAAAA  N NN **\n");	
	printf("** H  H  A   A  N  N  G   G  E     D D   M   M  A   A  N  N **\n");	
	printf("** H  H  A   A  N  N   GGG   EEEE  DD    M   M  A   A  N  N **\n");	
	printf("**************************************************************\n");	
	printf("\nversion 1.0\n\n");
}

/**
 * @brief Show the score for the two player
 *
 * @return void
**/
void showScore() {
	printf("Score: %s\t\t%d \n", p1.name, p1.score);
	printf("       %s\t\t%d \n\n", p2.name, p2.score);
}

