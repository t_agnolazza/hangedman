//#define DEBUG

/**
 * @file net.c
 * @brief Library with functions for sending and reeiving string/char through an open socket.
 *		  It includes also a function to retrieve ipAddress from open interfaces.
 *
 * @author T.Agnolazza
 * @version 1.0
**/

//Include basic libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Include network libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

//Retrieves net interfaces
//Not in gnu libc
#include <ifaddrs.h>

#include <limits.h>
#include "net.h"


/**
 * @brief Send a control character through the socket.
 *
 * @param socket an open socket
 * @param code the char you want to send 
 *
 * @return 0 if the char is send correctly, -1 if there is an error
**/
int sendCtrl(const int socket, const char code){
	if(send(socket, &code, sizeof(char), 0) != (sizeof(char))) {
		errNet(errno);
		printf("Error sending data.\n\n");
		return -1;
	}
	return 0;
}

/**
 * @brief Acquire a control charactter through the socket.
 *
 * @param socket an open socket
 *
 * @return the control char, -1 if there is an error
**/
char recCtrl(const int socket){
	char *buf = (char *) malloc(sizeof(char));
	if(recv(socket, buf, sizeof(char), 0) < 0){
		errNet(errno);
		printf("Error retrieving data.\n\n");
		free(buf);
		return -1;
	}
	char rec = buf[0];
	free(buf);
	return rec;
}

/**
 * @brief An esaier way to send on array of char.
 *
 * @param socket an open socket
 * @param buf array of char to be sent
 * @param length number of char to send
 *
 * @return 0 if the string in bus is send correctly, -1 otherwise
**/
int sendTo(const int socket, char *buf, int length) {
	if(send(socket, buf, sizeof(char)*length, 0) != (sizeof(char)*length)) {
		errNet(errno);
		printf("Error sending data.\n\n");
		return -1;
	}
#ifdef DEBUG
	printf("@@@ buf = %s\n", buf);
#endif
	memset(buf, '\0', length*sizeof(char));
	return 0;
}

/**
 * @brief An easier way to receive a string.
 *
 * @param socket an open socket
 * @param buf buffer array of length length
 * @param length number of char avaiable in buffer
 *
 * @return legth The actual length of the string received, -1 otherwise
*/
int recFrom(const int socket, char *buf, int length) { 
	memset(buf, '\0', length*sizeof(char));
	int bufSize = length;
	if((length = recv(socket, buf, sizeof(char)*length, 0)) < 0){
		errNet(errno);
		printf("Error retrieving data.\n\n");
		return -1;
	}
#ifdef DEBUG
	printf("@@@ buf = %s\n", buf);
#endif
	memset(buf+length, '\0', (bufSize-length)*sizeof(char));
	return length;
}

/**
 * @brief Ask to user and change the port in addr
 *
 * @param addr the pointer of the addr where there is addr.sin_port
 *
 * @return 0 if there were no problems, -1 otherwise
 *
 * @since 1.1
**/
int changingPort(struct sockaddr_in *addr){
	printf("Your actual port is %d\n", ntohs((*addr).sin_port));
	printf("What would you like (%d - %d)? ",IPPORT_RESERVED ,IPPORT_USERRESERVED);
	
	short newPort = 0;
	scanf("%hd", &newPort);
	char temp;
	while((temp = getchar()) != '\n' && (temp != '\r')) {}
	
	if((newPort > IPPORT_USERRESERVED) || (newPort < IPPORT_RESERVED)){
		printf("Out of range.\n\n");
		return -1;
	}
	
	(*addr).sin_port = htons(newPort);
	
#ifdef DEBUG
	printf("@@@ newPort = %d\n\n", ntohs((*addr).sin_port));
#endif
	return 0;
}

/**
 * @brief It prints every ip addr associated with an interface in this system
 *
 * @return 0 if there were no problems, -1 otherwise
 *
 * @since 1.1
**/
int getIpList(void){
	struct ifaddrs *ipAddr, *tmp;
	struct sockaddr_in *inetAddr; 
	
	if (getifaddrs(&ipAddr) < 0){
		return -1;
	}
	tmp = ipAddr;
	

	while(tmp != NULL){
		if (((*tmp).ifa_addr != NULL) && ((*(*tmp).ifa_addr).sa_family == AF_INET)) {
			inetAddr = (struct sockaddr_in *) (*tmp).ifa_addr;
			printf("Hostname: %s, address: %s\n", (*tmp).ifa_name, inet_ntoa((*inetAddr).sin_addr));
		}		
		tmp = (*tmp).ifa_next;
	}
	freeifaddrs(ipAddr);
	printf("\n");
	return 0;
}

/**
 * @brief 
 *
 * @return 
**/
void errNet(int errorCode){
}
