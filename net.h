/**
 * @file net.c
 * @brief Library with functions for sending and reeiving string/char through an open socket.
 *		  It includes also a function to retrieve ipAddress from open interfaces.
 *
 * @author T.Agnolazza
 * @version 1.1
**/

/**
 * @brief Send a control character through the socket.
 *
 * @param socket an open socket
 * @param code the char you want to send 
 *
 * @return 0 if the char is send correctly, -1 if there is an error
**/
int sendCtrl(const int socket, const char code);

/**
 * @brief Acquire a control charactter through the socket.
 *
 * @param socket an open socket
 *
 * @return the control char, -1 if there is an error
**/
char recCtrl(const int socket);

/**
 * @brief An esaier way to send on array of char.
 *
 * @param socket an open socket
 * @param buf array of char to be sent
 * @param length number of char to send
 *
 * @return 0 if the string in bus is send correctly, -1 otherwise
**/
int sendTo(const int socket, char *buf, int length);

/**
 * @brief An easier way to receive a string.
 *
 * @param socket an open socket
 * @param buf buffer array of length length
 * @param length number of char avaiable in buffer
 *
 * @return legth The actual length of the string received, -1 otherwise
*/
int recFrom(const int socket, char *buf, int length);

/**
 * @brief Ask to user and change the port in addr
 *
 * @param addr the pointer of the addr where there is addr.sin_port
 *
 * @return 0 if there were no problems, -1 otherwise
 *
 * @since 1.1
**/
int changingPort(struct sockaddr_in *addr);

/**
 * @brief It prints every ip addr associated with an interface in this system
 *
 * @return 0 if there were no problems, -1 otherwise
 *
 * @since 1.1
**/
int getIpList(void);

/**
 * @brief 
 *
 * @return 
**/
void errNet(int errorCode);


