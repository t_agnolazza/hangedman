//#define DEBUG
#define DIM_BUFFER 65
#define DIM_WORD 27
#define STR_DIM_WORD "27"
#define DIM_PACKET 7
#define MAX_TRY 6 //number of tries before hanging

/**
 * @file hanged.c
 * @brief It contains the two function of the routine of the game
 *
 * @author T.Agnolazza
 * @version 1.0
**/

//Include basic libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Include network libraries
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#include "hanged.h"
#include "net.h"


/**
 * @brief Choosing the word
 * @detail It asks the user to insert the word to guess. 
 *		   It sends the word to the other player.
 *		   It receives and prints every choice of the other player until he guess or loose all his lives.
 *
 * @param socket an open socket
 *
 * @return 'w' if the other did not guess, 'n' otherwise; 'e' if there were error
**/
char chooseWord(int socket){

	//Initializing buffer of strings
	char **toRec = (char **)malloc(DIM_PACKET*sizeof(char *));
	for(int i = 0; i < DIM_PACKET; i++) {
		toRec[i]= (char *)malloc(DIM_BUFFER*sizeof(char));
	}
	
	char ans = '\0';
	int length = 0;
	
	//Choosing a word
	printf("\n\nChoose a word: ");
	scanf("%"STR_DIM_WORD"[a-z]*s", toRec[0]);
				
#ifdef DEBUG
	printf("\n--- buf: - %s -\n\n", toRec[0]);
#endif
	
	//Sending the word
	if(sendTo(socket, toRec[0], strlen(toRec[0])) < 0) {
		return errno;
	}
	
	printf("\nThe other:\n\n");
	
	while((ans!='w') && (ans!='l')){
		
		for(int i = 0; i < DIM_PACKET;i++){
		
			//Receving the length of the next packet
			if((length=(int) recCtrl(socket)) < 0){
				free(toRec);
				return 'e';
			}
			
			//Receiving the packet
			if(recFrom(socket, toRec[i], length) < 0){
				free(toRec);
				return 'e';
			}
			
			//Printing the block of code
			printf("%s", toRec[i]);
		#ifdef DEBUG
			printf("---buf:%s\n", toRec[i]);
		#endif
		
			//Erasing the buffer
			memset(toRec[i], '\0', DIM_BUFFER*sizeof(char));
		}
		
		//Ans is reffered to the other player
		if((ans=recCtrl(socket)) < 0){
			free(toRec);
			return 'e';
		}

	#ifdef DEBUG
		printf("\n--- ans: %c\n\n", ans);
	#endif
	}
	
	free(toRec);
	
	if(ans == 'l'){
		ans = 'w';
	}
	else if (ans == 'w'){
		ans = 'l';
	}
	return ans;
}

/**
 * @brief Guessing the word
 * @detail It receives the word form the other player.
 *		   It enters a loop, which ends if the player guesses the word or he lost all of his lives.
 *		   The player can either chooses to guess the word or to guess a letter.
 *		   Every miss is a life lost.
 *		   Every choices is registered into a string buffer and sent to the other player.
 *
 * @param socket an open socket
 *
 * @return 'w' if you guess, 'n' otherwise; 'e' if there were error
**/
char guess(int socket){
	char *buf = (char *) malloc(DIM_BUFFER*sizeof(char));
	char **toSend = (char **) malloc(DIM_PACKET*sizeof(char *));
	for (int i = 0; i<DIM_PACKET; i++) {
		toSend[i] = (char *) malloc (DIM_BUFFER*sizeof(char));
	}
	
	char *toGuess = (char *) malloc(DIM_BUFFER*sizeof(char));
	char *word = (char *) malloc(DIM_WORD*sizeof(char));
	char ans = '\0';
	int try = MAX_TRY;
	int freq = 0;
	int count = 0;
	
	//Introducing the guessing phase
	printf("\n\nYou have to guess!\n");
	
	//Retrieving the word
	if(recFrom(socket, word, DIM_WORD) < 0) {
		free(buf);
		free(toSend);
		free(word);
		free(toGuess);
		return 'e';
	}
	
	//Creating the help "____..."
	for(int i = 0; i<strlen(word); i++){
		toGuess[i]='_';
	}
	toGuess[strlen(word)]='\0';
	
	printf("\n\n- %s -\t%d letters.\n\n", toGuess, strlen(toGuess));
	
	#ifdef DEBUG
		printf("--- word: - %s -\n", word);
		printf("--- strlen(word): %d\n\n", strlen(word));
		printf("--- toGuess: - %s -\n", toGuess);
		printf("--- strlen(toGuess): %d\n\n", strlen(toGuess));
	#endif

	char tmp;
	
	//Beginning the long "word or letter cycle"
	while((ans!='w') && (ans!='l')){
	
		printf("Do you want to guess the word or a letter (w/l)? ");	
		scanf("%1s", buf);
		
		while((tmp = getchar()) != '\n' && (tmp != '\r')) {}
			
		//Writing text for the other player
		sprintf(toSend[count++], "Do you want to guess the word or a letter (w/l)? %s\n", buf);
		
		if((buf[0] == 'w')||(buf[0] == 'W')) {
		
			memset(buf, '\0', sizeof(char));
		
			//Guess the word
			printf(" :");
			scanf("%"STR_DIM_WORD"[A-z]s", buf);
			
			for(int i = 0; i<strlen(buf); i++){
				buf[i] = tolower(buf[i]);
			}
			
			while((tmp = getchar()) != '\n' && (tmp != '\r')) {}
			
			sprintf(toSend[count++], ": %s\n", buf); 
			
			//Are you wright?
			if(strcmp(buf, word) == 0){
				printf("Great! :%s is correct!\n\n", buf);
				sprintf(toSend[count++], "Great! :%s is correct!\n\n", buf); 

				ans = 'w';	
			}
			
			//Are you wrong?
			else {
				printf("It's not the word.\n\n");
				
				sprintf(toSend[count++], "It's not the word.\n");

				if((--try) <= 0){
					printf("Hanged!\n\n");
					sprintf(toSend[count++], "Hanged!\n\n");
					
					ans = 'l';
				}
				
				else{
					printf("You've left %d tries left.\n\n", try);
					sprintf(toSend[count++], "You've left %d tries left.\n", try);
				}	
				
			}
			
		}
	
		else if((buf[0] == 'l')||(buf[0] == 'L')) {
		
			memset(buf, '\0', sizeof(char));
						
			//Try with a letter
			printf(" :");
			scanf("%1[A-z]s", buf);
	
			while((tmp = getchar()) != '\n' && (tmp != '\r')) {}
			
			buf[0]=tolower(buf[0]);
			
			sprintf(toSend[count++], " : %c\n", buf[0]);
			
			//Refreshing the wholy word
			for(int i = 0; i<strlen(toGuess); i++) {
				if(buf[0]==word[i]){
					freq++;
					toGuess[i]=buf[0];
				}
			}
			
			printf("\'%c\' has frequency %d.\n", buf[0], freq);
			sprintf(toSend[count++], "\'%c\' has frequency %d.\n", buf[0], freq);
												
			if(freq<=0){
			
				//If that letter is not in the word
				printf("That letter is not in the word.\n\n");
				sprintf(toSend[count++], "That letter is not in the word.\n\n");
				
				if((--try)<=0) {
					printf("Hanged!\n\n");
					sprintf(toSend[count++], "Hanged!\n\n");
					
					ans = 'l';
				}
				
				else {
					printf("You've left %d tries left.\n", try);
					printf("- %s -\n\n", toGuess);
					
					sprintf(toSend[count++], "You've left %d tries left.\n", try);
					sprintf(toSend[count++], "- %s -\n\n", toGuess);
				}						
			}
			
			else {
				printf("- %s -\n\n", toGuess);	
				sprintf(toSend[count++], "- %s -\n\n", toGuess);	
				
				if(strcmp(toGuess, word) == 0){
					printf("Great! :%s is correct!\n\n", toGuess);
					sprintf(toSend[count++], "Great! :%s is correct!\n\n", toGuess); 
	
					ans = 'w';	
				}
			}
			
			//Reset
			freq = 0;
		}
		
		//Input not valid
		else{
			printf("I can not understand what you have written.\n\n");
			sprintf(toSend[count++], "I can not understand what you have written.\n\n");
		}
		
		if(ans == 'l') {
			sprintf(toSend[count++], "The word was %s\n\n", word);
		}
		//Sending the whole packet to the other player
		for(int i = 0; i < DIM_PACKET; i++){
		
			if(sendCtrl(socket, (char) strlen(toSend[i])) < 0){
				free(buf);
				free(toSend);
				free(word);
				free(toGuess);
				return 'e';			
			}
			
		#ifdef DEBUG
			printf("---%s", toSend[i]);
			printf("---%d\n\n", strlen(toSend[i]));
		#endif
			if(sendTo(socket, toSend[i], strlen(toSend[i])) < 0){
				free(buf);
				free(toSend);
				free(word);
				free(toGuess);
				return 'e';		
			}
			memset(toSend[i], '\0', DIM_BUFFER*sizeof(char));
		}
		
		count = 0;
		
	#ifdef DEBUG
		printf("---ans = %c\n\n", ans);
	#endif
	
		if(sendCtrl(socket, ans) < 0){
				free(buf);
				free(toSend);
				free(word);
				free(toGuess);
				return 'e';			
		}
	}
	
	free(buf);
	free(toSend);
	free(word);
	free(toGuess);
	return ans;	
}
